#Flywhel REDCap Services

## REDCap to Flywheel Data Transfer

### Overview

This service is designed to transfer data from REDCap to Flywheel. It is designed to be run as a cron job on a regular schedule. The service will connect to a REDCap project and download the most recent data and then upload it to a Flywheel project. The service is designed to be run as a Docker container and is available on Docker Hub. 

### Usage

usage: run_redcap2flywheel.py [-h] [-r RCAPI] [-u RCURL] [-f FWAPI] [-p PROJECTID] [-y YAMLFILE]

Uploads metadata from a redcap instance to a flywheel project

```bash 
arguments:
  -h, --help            show this help message and exit
  -r RCAPI, --rcAPI RCAPI
                        The API key for the source REDCap instance that you are downloading data from.
  -u RCURL, --rcURL RCURL
                        The URL of the source REDCap instance that you are downloading data from.
  -f FWAPI, --fwapi FWAPI
                        The API key for the target Flywheel instance that you are uploading data to
  -p PROJECTID, --projectID PROJECTID
                        The ID of the target Flywheel project you are uploading data to.
  -y YAMLFILE, --yamlFile YAMLFILE
                        The full path to the yaml mapping file that describes what metadata to download_
```

### Workflow

The overall workflow is as follows:
1. Obtain a Flywheel API key
2. Obtain a REDCap API key
3. Ensure that all flywheel Subjects have a metadata field that contains their
corresponding REDCap RecordID
4. Obtain a list of all REDCap Fields that you wish to import to flywheel.
5. Create a REDCap to Flywheel mapping yaml file.
6. Execute the script.

The script will look in a specified location on Flywheel subjects for their 
corresponding REDCap RecordID.  It will then find the correct Record that 
matches this ID, and copy over any specified fields to that subject. 

```mermaid
flowchart TB
    subgraph Service
        direction TB

        subgraph Initialize
            direction LR
                A[Flywheel API key] --> B((REDCap to Flywheel \nService))
                AA[REDCap API key] --> B
                AAA[Mapping Yaml file] --> B
                B -->|REDCap API| D[REDCap]
                D -->|Project Records| B
                B -->|Flywheel API| E[Flywheel]
                E -->|Flywheel Subjects| B
        end
        Initialize -->|REDCap Records| Process[Record Iterator] 
        Initialize -->|Flywheel Subjects| Process[Record Iterator] 
        
        subgraph Process
            direction LR
                RR[REDCap Records] --> IT
                F{Record\nMatch?} -->|REDCap Fields| EE[Flywheel]
                IT(((Record Iterator))) -->|REDCap Record ID| F
                FS[Flywheel Subjects] --->|Flywheel Subject| F
        end

    end

```

### Components

#### rcAPI

The REDCap API can be found on your redcap instance.  You must be approved for 
API access from a REDCap administrator.  Once approved, you will be able to
retrieve your API key from the redcap instance.

You can view your API key from the main REDCap menu by clicking on "API":

<img src="./map_redcap_to_flywheel_service/images/REDCap_menu_API.png" width="250"/>

<img src="./map_redcap_to_flywheel_service/images/REDCap_API_token.png" width="500"/>


#### rcURL

The redcap url is the web address of the redcap instance you're on, but specific
to API requests.  You can generate this yourself or copy it from the API
documentation.  

 - **Manual Extraction**: When viewing your redcap project, copy the URL up to the
first slash.  For example, if you're at your project's home page and the url in
your browser is:

    `https://redcap.flywheel.io/redcap_v13.4.12/index.php?pid=585`

    Then you would copy: `https://redcap.flywheel.io`

    Now just add "/api/" to the end of it, so your rcURL would be:
`https://redcap.flywheel.io/api/`

 - **From API Docs**: Click on the `API Playground` link shown in the menu 
above, and click on the `REDCap API documentation` link.

    <img src="./map_redcap_to_flywheel_service/images/REDCAP_Playground_Docs.png" width="600"/>
   
    From here, click on any item in the left-hand menu, below the
 `Supported Methods`  subgroup, and copy the url from the "URL" section on the 
right

      <img src="./map_redcap_to_flywheel_service/images/REDCap_Getting_APIURL.png" width="400"/>
    

#### fwAPI
 This is your flywheel API key.  [Details on how to get that are here.](https://docs.flywheel.io/Administrator_Guides/admin_creating_a_user_api_key/)


#### projectID
 The ID of the project you want to import the redcap data into.  If you are 
 viewing the project, the ID will be in the URL of your browser:
 
<img src="./map_redcap_to_flywheel_service/images/REDCAP_Flywheel_projectID.png" width="450"/>


#### yamlFile
 This is the mapping yaml file that describes which REDCap fields get imported
 to flywheel, and how to map subjects in flywheel to subjects in redcap. 
 
### Instructions

#### Setting up the yaml mapper

The first thing you need to do is prepare your yaml file.  The yaml maps redcap
to flywheel and indicates which fields should be copied over. 

##### REDCap Record ID
Every question/input in redcap is called a "field", and each item has a unique
field name.  The field names are different from what you see on the forms 
themselves.  One field name, called the "Record ID" is the most important, as 
it is a field that is unique for every record.  This field is usually used to 
identify a unique subject, and all other fields this person fills out get
associated with their Record ID.  

To determine the field name of your projects record ID, go to the "Codebook"
on the left-hand navigation menu:

<img src="./map_redcap_to_flywheel_service/images/RedCAP_menu_Codebook.png" width="250"/>

From there, you can identify the field name from the `Variable/field Name` 
column. The Field Label should be "Record ID", and it should be the first item 
in the list. In this case, the Record ID field name is `record_id`, but it could
be anything.

<img src="./map_redcap_to_flywheel_service/images/REDCap_Codebook_RecordID.png" width="500"/>

This codebook also contains every field name/label for all items
in the entire project.  It's a good resource, but if you want to copy ALL the
fields to flywheel, there's a better way to get the full list of fields.


##### Getting all REDCap fields in a project
To get ALL the fields in a project, we can navigate to the API playground:

<img src="./map_redcap_to_flywheel_service/images/REDCap_menu_API.png" width="250"/>

And:
1. select the `Export List of Export Field Names` API Method.
2. click "Execute Request"

Below this button you'll see a JSON list of all the field names.  
From here you can use python or some other json parsing tool to extract 
a full list of field names.  


<img src="./map_redcap_to_flywheel_service/images/REDCap_APIplayground_fields.png" width="500"/>

This request returns a list, where there's one object for each field.
The field objects have the format:

```json 
{
    "original_field_name":<field_id>,
    "choice_value":<choice_value>,
    "export_field_name":<export_field_id>
}
```

The only value you're interested is `original_field_name`.  Extract these
values to get a full list of the fields in a project.

##### Yaml Setup

Now you're finally ready to set up the yaml map.  

Here is an example yaml that walks you through each component:

```yaml
# This describes where we will find the correct REDCAP
# "record ID" for metadata at the subject level
record:

  # First we indicate what the redcap record ID label is:
  # In this case the label is "record_id" in redcap.
  rc_key: record_id

  # Now we indicate where the VALUE of this redcap record ID is
  # within flywheel:
  fw_key: info.REDCapID

  # Finally we must know which container this fw_key is on:
  container_level: subject

  # Together, this tells us that the unique redcap record field
  # is "record_id" in redcap.  It also tells us that in flywheel,
  # there will be a field that matches the value of this rc_subid
  # at subject.info.REDCapID.  We can use this to match a flywheel subject
  # with a redcap record.

# This is the meat of the mapping.  It specifies which 
# redcap metadata to import. test
fields:

  # The RC items listed here will be copied over to each subject
  - <field_id 1>
  - ...
  - <field_id N>
```

###### record
The record section helps link REDCap records to flywheel subjects.

The two elements that link the flywheel subject and the redcap record are:
1. rc_key
2. fw_key

The rc_key tells us which field is the REDCap Record id.
As we found out in the earlier steps, it's "record_id".

The fw_key tells us where in flywheel we can find the subject's REDCap 
record id.  For example, if a redcap record has a record_id of "P0123", 
we would need to put "P0123" somewhere on the corresponding subject's 
metadat in flywheel.  If we add this to the metadata key `subject.info.REDCapID`
, we would set the fw_key to `info.REDCapID`.  To let the script know
that this key is on the subject leve, we specify `container_level`: subject.

Our subject would need a field in the `Custom Information` tab that contained
this value:

<img src="./map_redcap_to_flywheel_service/images/REDCap_Flywheel_subject_metadata.png" width="600"/>

This tells the script to look at `subject.info.REDCapID` to get the REDCap
record_id, and will then use that id to find the correct REDCap record for this
subject. 


###### fields:
 This is a list of REDCap fields to be ingested to the subject.  USE CAUTION 
 when importing all fields, as REDCap forms may contain PHI.  