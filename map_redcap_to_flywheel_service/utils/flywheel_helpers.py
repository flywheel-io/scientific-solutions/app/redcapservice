import logging
import re
CLEANR = re.compile('<.*?>')

log = logging.getLogger(__name__)
log.setLevel('DEBUG')


def get_name(container):
    ct = container.container_type
    
    if ct == 'file':
        name = container.name
    else:
        name = container.label
    return(name)




def cleanhtml(raw_html):
  cleantext = re.sub(CLEANR, '', raw_html)
  return cleantext